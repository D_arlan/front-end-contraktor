import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Login from './pages/Login'
import Contratos from './pages/App'
import Register from './pages/Register'
import Contrato from './pages/Contrato'
import NewContrato from './pages/NewContrato'

export default function Routes() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Login} />
                <Route path="/register" exact component={Register} />
                <Route path="/contratos" exact component={Contratos} />
                <Route path="/contrato/:id" exact component={Contrato} />
                <Route path="/novo-contrato" exact component={NewContrato} />
            </Switch>
        </BrowserRouter>
    )
}