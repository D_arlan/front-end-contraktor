import React, {useState, useEffect} from 'react';
import { format, parseISO } from 'date-fns';


import { useParams } from 'react-router-dom';

import api from '../../services/api'

import { FiPower, FiUser, FiCalendar, FiFileText, FiArrowLeft, FiPaperclip, FiNavigation} from 'react-icons/fi';
import { Link, useHistory } from 'react-router-dom'




import './style.css'
import logo from '../../assets/logo.png';
import avatar from '../../assets/avatar.png';



function Contratos() {


    const [contrato, setContrato] = useState([])
    const [partes, setPartes] = useState([])
    const userName = localStorage.getItem('userName')

    const [cpf, setCpf] = useState([])
    const { id } = useParams();       

    useEffect(() => {
        api.get(`/contratos/partes/list/${id}`).then( response =>{
            setPartes(response.data)
        })
        api.get(`contratos?id=${id}`).then(response => {
            setContrato(
                {
                    datainicial:format(parseISO(response.data[0].datainicial), 'dd/MM/yyyy'),
                    vencimento:format(parseISO(response.data[0].vencimento), 'dd/MM/yyyy'),
                    titulo:response.data[0].titulo
                }
            )
        })
    }, [id, partes])


    const history = useHistory()
    
    function hadleLogout() {
        localStorage.clear()
        history.push('/')
    }
    
    function hadleInsertParte(e){
        e.preventDefault()
            const data = {cpf, contrato_id:id}
            api.post(`/contratos/partes`, data)
    }

  return (
      <div className="content-contrato">
          <header>
            <img className="logo" src={logo} alt="Contractor" />
            <div className="profile">
                <img className="avatar" src={avatar} alt="Contractor" />
                <span>{userName}</span>
            </div>
            <FiPower onClick={hadleLogout} size={18} style={{cursor:'pointer'}} color="#E02041" />
          </header>
          
          <main>
                <Link to="/contratos" className="button-voltar"><FiArrowLeft /> Voltar</Link>
                <div className="content-colums">
                    <div className="colum-contrato">
                        <h1><FiFileText /> {contrato.titulo}</h1>
                        <span><Link className="doc"><FiPaperclip /> Ver Contrato</Link></span>
                        
                        
                        <div className="group-data">
                            <h3><FiCalendar size={18} color="#377dff" /> Data de inicio: {contrato.datainicial}</h3>
                            <h3><FiCalendar size={18} color="#E02041" /> Vencimento: {contrato.vencimento}</h3>
                        </div>
                        
                    </div>
                    <div className="colum-partes">
                    <h2 style={{marginBottom:20}}>Partes relacionadas:</h2>
                    <p>Informe o CPF para cadastrar uma parte ao contrato</p>
                    <div className="search">
                        <form className="cadastro-partes" onSubmit={hadleInsertParte}>
                            <input 
                            type="text" 
                            placeholder="Informe o CPF"
                            value={cpf}
                            onChange={e => setCpf(e.target.value)}
                            />
                            <button><FiNavigation size={20} color="#377dff" /></button>
                        </form>
                    </div>
                        <ul>
                        <li>
                                <div className="icon">
                                    <FiUser size={30} color="#377dff" />
                                </div>
                                <div className="content-item">
                                <h2>Drlan Costa</h2>
                                    <p style={{marginTop:5, color:'#444'}}>Parte relacionada</p>
                                    <p>CPF: 608.747.577-77 <br/> Telefone: (85)99928-6979</p>
                                </div>
                            </li>
                            <li>
                                <div className="icon">
                                    <FiUser size={30} color="#377dff" />
                                </div>
                                <div className="content-item">
                                <h2>Drlan Costa</h2>
                                    <p style={{marginTop:5, color:'#444'}}>Parte relacionada</p>
                                    <p>CPF: 608.747.577-77 <br/> Telefone: (85)99928-6979</p>
                                </div>
                            </li>
                            <li>
                                <div className="icon">
                                    <FiUser size={30} color="#377dff" />
                                </div>
                                <div className="content-item">
                                <h2>Drlan Costa</h2>
                                    <p style={{marginTop:5, color:'#444'}}>Parte relacionada</p>
                                    <p>CPF: 608.747.577-77 <br/> Telefone: (85)99928-6979</p>
                                </div>
                            </li>
                            <li>
                                <div className="icon">
                                    <FiUser size={30} color="#377dff" />
                                </div>
                                <div className="content-item">
                                <h2>Drlan Costa</h2>
                                    <p style={{marginTop:5, color:'#444'}}>Parte relacionada</p>
                                    <p>CPF: 608.747.577-77 <br/> Telefone: (85)99928-6979</p>
                                </div>
                            </li>

                        {
                        partes.map(parte => (
                            <li>
                                <div className="icon">
                                    <FiUser size={30} color="#377dff" />
                                </div>
                                <div className="content-item">
                                <h2>{parte.nome} {parte.sobrenome}</h2>
                                    <p style={{marginTop:5, color:'#444'}}>Parte relacionada</p>
                                    <p>CPF: {parte.cpf} <br/> Telefone: {parte.telefone}</p>
                                </div>
                            </li>
                        ))}
                        </ul>
                    </div>
                </div>
                
          </main>
      </div>
  );
}

export default Contratos;