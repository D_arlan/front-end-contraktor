import React, {useState, useEffect} from 'react';
import api from '../../services/api'

import { Link, useHistory } from 'react-router-dom'
import { FiLogIn } from 'react-icons/fi'


import './style.css';
import loginImg from '../../assets/imagem-login.png';
import logo from '../../assets/logo.png';


function Login() {

    const [cpf, setCpf] = useState('')
    const history = useHistory()


    useEffect(() => {
      //history.push('/contratos')
      // if (localStorage.getItem('userId')) {
      //   history.push('/contratos')
      // }
  })

   
    
    async function handleLogin(e) {
        e.preventDefault();
        history.push('/contratos')
        try {
            const response = await api.get(`partes?cpf=${cpf}`)
            if(response.data[0].cpf){
              localStorage.setItem('userCpf', response.data[0].cpf)
              localStorage.setItem('userId', response.data[0].id)
              localStorage.setItem('userName', response.data[0].nome)
              history.push('/contratos')
            }
        }
        catch (err) {
            alert('Falha no login, tente novamente')
        }
    }
  return (
    <div className="container">
      <div className="content first-content">
        <div className="first-column">
          <img src={logo} alt="Contractor" />
          <h2 className="title">Bem vindo!</h2>
          <p className="description">Informe seu CPF</p>
          <form onSubmit={handleLogin}>
            <input type="text" 
            placeholder="Seu CPF"
            value={cpf}
            onChange={e => setCpf(e.target.value)}
            />
            <button 
          className="button"
          type="submit"
          >Entrar</button>
          </form>
          
          <Link className="link-register" to="register"><FiLogIn size={16} color="#444" /> Registrar-se</Link>
        </div>
        <img className="img-login" src={loginImg} alt="Contractor" />
      </div>
    </div>
  );
}

export default Login;