import React, {useState} from 'react';
import { Link } from 'react-router-dom'
import { FiLogIn } from 'react-icons/fi'
import api from '../../services/api'
import { useHistory } from 'react-router-dom'


import './style.css';
import logo from '../../assets/logo.png';


function Login() {
  const history = useHistory()

  const [nome, setNome] = useState();
  const [sobrenome, setSobrenome] = useState();
  const [cpf, setCpf] = useState();
  const [telefone, setTelefone] = useState();

  async function hadleNewParte(e) {
    e.preventDefault();
    const data = {
        nome,
        sobrenome,
        cpf,
        telefone,
    }
    //console.log(data);
    try {
        await api.post('partes', data)
        history.push('/')
    } catch (err) {
        alert('Erro ao cadastrar tente novamente!')
    }

}
  return (
    <div className="container">
      <div className="content-register">
      <img className="logo" src={logo} alt="Contractor" />
          <h2 className="title">Registre-se!</h2>
          <form onSubmit={hadleNewParte}>
              
            <input 
              type="text" 
              placeholder="Nome"
              value={nome}
              onChange={e => setNome(e.target.value)}
            />
            <input 
              type="text" 
              placeholder="Sobrenome"
              value={sobrenome}
              onChange={e => setSobrenome(e.target.value)}
            />
            <div className="group-input">
                <input 
                  type="text" 
                  placeholder="CPF"
                  value={cpf}
                  onChange={e => setCpf(e.target.value)}
                />
                <input 
                  type="text" 
                  placeholder="telefone"
                  telefone={telefone}
                  onChange={e => setTelefone(e.target.value)}
                />
            </div>
            
            <button className="button" type="submit">Registrar-se</button>
          </form>
          
          <Link className="link-register" to="/"><FiLogIn size={16} color="#444" /> Login</Link>
      </div>
    </div>
  );
}

export default Login;