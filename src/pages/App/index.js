import React, {useState, useEffect} from 'react';
import { format, parseISO } from 'date-fns';

import api from '../../services/api'

import { FiPower, FiTrash2, FiArchive, FiSearch} from 'react-icons/fi';
import { Link, useHistory } from 'react-router-dom'




import './style.css'
import logo from '../../assets/logo.png';
import avatar from '../../assets/avatar.png';



function App() {

    const [contratos, setContratos] = useState([])
    const [search, setSearch] = useState([])
    const userCpf = localStorage.getItem('userCpf')
    const userId = localStorage.getItem('userId')
    const userName = localStorage.getItem('userName')

    useEffect(() => {
        try {
            api.get(`contratos?cpf=${userCpf}`).then(response => {
                //console.log(response);
                setContratos(response.data)
                
            })
        } catch (error) {
        }
        
    }, [userCpf])
    const history = useHistory()
    function hadleLogout() {
        localStorage.clear()
        history.push('/')
    }
    function hadleSearchContract(e){
        e.preventDefault()
            const data = {
                id_parte:userId, 
            }
            api.post(`/contratos/search/?p=${search}`, data).then(response => {
                console.log(response);
                setContratos(response.data)
                
            })
    }



  return (
      <div className="content-app">
          <header>
            <img className="logo" src={logo} alt="Contractor" />
            <div className="profile">
                <img className="avatar" src={avatar} alt="Contractor" />
                <span>{userName}</span>
            </div>
            <FiPower onClick={hadleLogout} size={18} style={{cursor:'pointer'}} color="#E02041" />
          </header>
          
          <main>
                <h1>Contratos:</h1>
                <div className="header-main">
                <div className="search">
                    <form onSubmit={hadleSearchContract}>
                        <input 
                            placeholder="Pesquisar contrato" 
                            type="text"
                            value={search}
                            onChange={e => setSearch(e.target.value)}
                        />
                        <button><FiSearch size={20} color="#377dff" /></button>
                    </form>
                </div>
                <Link className="button" to="novo-contrato">Cadastrar contrato</Link>
                </div>
                <ul>


<li>
                        <div className="icon">
                        <Link style={{textDecoration:'none', color:'#444'}} ><FiArchive size={30} color="#377dff" /></Link>
                        </div>
                        <div className="content-item">
                            <h2><Link style={{textDecoration:'none', color:'#444'}} >Contrato de Locação de veiculos</Link></h2>
                            <p style={{marginTop:5, color:'#444'}}>Contrato</p>
                            <p>Inicio: 22/12/2020</p>
                            <p>Validade: 22/12/2025</p>
                        </div>
                        <button type="button">
                            <FiTrash2 size={20} color="#A8AD3" />
                        </button>
                        
                    </li>

                    <li>
                        <div className="icon">
                        <Link style={{textDecoration:'none', color:'#444'}} ><FiArchive size={30} color="#377dff" /></Link>
                        </div>
                        <div className="content-item">
                            <h2><Link style={{textDecoration:'none', color:'#444'}} >Contrato de Locação de veiculos</Link></h2>
                            <p style={{marginTop:5, color:'#444'}}>Contrato</p>
                            <p>Inicio: 22/12/2020</p>
                            <p>Validade: 22/12/2025</p>
                        </div>
                        <button type="button">
                            <FiTrash2 size={20} color="#A8AD3" />
                        </button>
                        
                    </li>

                    <li>
                        <div className="icon">
                        <Link style={{textDecoration:'none', color:'#444'}} ><FiArchive size={30} color="#377dff" /></Link>
                        </div>
                        <div className="content-item">
                            <h2><Link style={{textDecoration:'none', color:'#444'}} >Contrato de Locação de veiculos</Link></h2>
                            <p style={{marginTop:5, color:'#444'}}>Contrato</p>
                            <p>Inicio: 22/12/2020</p>
                            <p>Validade: 22/12/2025</p>
                        </div>
                        <button type="button">
                            <FiTrash2 size={20} color="#A8AD3" />
                        </button>
                        
                    </li>

                    <li>
                        <div className="icon">
                        <Link style={{textDecoration:'none', color:'#444'}} ><FiArchive size={30} color="#377dff" /></Link>
                        </div>
                        <div className="content-item">
                            <h2><Link style={{textDecoration:'none', color:'#444'}} >Contrato de Locação de veiculos</Link></h2>
                            <p style={{marginTop:5, color:'#444'}}>Contrato</p>
                            <p>Inicio: 22/12/2020</p>
                            <p>Validade: 22/12/2025</p>
                        </div>
                        <button type="button">
                            <FiTrash2 size={20} color="#A8AD3" />
                        </button>
                        
                    </li>

                    <li>
                        <div className="icon">
                        <Link style={{textDecoration:'none', color:'#444'}} ><FiArchive size={30} color="#377dff" /></Link>
                        </div>
                        <div className="content-item">
                            <h2><Link style={{textDecoration:'none', color:'#444'}} >Contrato de Locação de veiculos</Link></h2>
                            <p style={{marginTop:5, color:'#444'}}>Contrato</p>
                            <p>Inicio: 22/12/2020</p>
                            <p>Validade: 22/12/2025</p>
                        </div>
                        <button type="button">
                            <FiTrash2 size={20} color="#A8AD3" />
                        </button>
                        
                    </li>



                {
                contratos.map(contrato => (
                    <li key={contrato.contrato_id}>
                        <div className="icon">
                        <Link style={{textDecoration:'none', color:'#444'}} to={`contrato/${contrato.contrato_id}`}><FiArchive size={30} color="#377dff" /></Link>
                        </div>
                        <div className="content-item">
                            <h2><Link style={{textDecoration:'none', color:'#444'}} to={`contrato/${contrato.contrato_id}`}>{contrato.titulo}</Link></h2>
                            <p style={{marginTop:5, color:'#444'}}>Contrato</p>
                            <p>Inicio: {format(parseISO(contrato.datainicial), 'dd/MM/yyyy')}</p>
                            <p>Validade: {format(parseISO(contrato.vencimento), 'dd/MM/yyyy')}</p>
                        </div>
                        <button type="button">
                            <FiTrash2 size={20} color="#A8AD3" />
                        </button>
                        
                    </li>
                ))}
                    
                    
                    
                    
                </ul>
          </main>
      </div>
  );
}

export default App;