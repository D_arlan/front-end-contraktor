import React, {useState, useEffect} from 'react';
import api from '../../services/api'
import { useHistory } from 'react-router-dom'

import './style.css';
import loginImg from '../../assets/imagem-login.png';

function NewContrato() {
    const history = useHistory()
    const [contractorFile, setContractorFile] = useState();
    const [titulo, setTitulo] = useState();
    const [dataInicio, setDataInicio] = useState();
    const [vencimento, setVencimento] = useState();
    const userId = localStorage.getItem('userId')


    const handleUploadFile = (e) => {
        setContractorFile(e.target.files[0]);
    }

    async function hadleNewContractor(e) {
        e.preventDefault();
        const data = {
            titulo,
            datainicial:dataInicio,
            vencimento,
            id_parte:userId,
            contractorFile
        }
        //console.log(data);
        try {
            await api.post('contratos', data)
            history.push('/contratos')
        } catch (err) {
            alert('Erro ao cadastrar contrato tente novamente!')
        }

    }
    
    useEffect(() => {
        
        console.log(contractorFile);
    }, [contractorFile])

  return (
      <div className="content-new-contrato">
            <div className="colum-one">
                <img className="img-login" src={loginImg} alt="Contractor" />
            </div>
            <div className="conlum-two">
                <h1>Novo contrato</h1>
                <p className="description">Cadastre um novo contrato</p>
                <form onSubmit={hadleNewContractor}>
                    <input 
                        type="text" 
                        placeholder="Titulo do Contrato"
                        value={titulo}
                        onChange={e => setTitulo(e.target.value)}
                    />
                    <div className="group-input">
                        <input 
                            type="date" 
                            placeholder="Data de inicio"
                            value={dataInicio}
                            onChange={e => setDataInicio(e.target.value)}
                        />
                        <input 
                            type="date" 
                            placeholder="Validade"
                            value={vencimento}
                            onChange={e => setVencimento(e.target.value)}
                        />
                    </div>
                    <label for="myfile">Selecione seu documento:</label>
                    <input type="file" 
                        id="myfile" 
                        name="myfile"
                        onChange={handleUploadFile}
                        accept="application/pdf"
                    />
                    <button className="button">Cadastrar</button>
                </form>
            </div>
      </div>
  );
}

export default NewContrato;